import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'enumerator'
})
export class EnumeratorPipe implements PipeTransform {
  transform(value: number, ...args: any[]): any {
    const enumerations: Array<number> = [];
    const valueInGroupsOfTen = value / 10;
    for (let i = 0; i < valueInGroupsOfTen; i++) {
      enumerations.push(i + 1);
    }
    return enumerations;
  }
}
