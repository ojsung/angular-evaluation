import { Component, OnInit, OnDestroy } from '@angular/core';
import { PersonHttpService } from '../person-http.service';
import { Subscription } from 'rxjs';
import { IPerson } from './person';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit, OnDestroy {

  constructor(private phs: PersonHttpService) { }
  private personListSubscription: Subscription;
  public pageTitle = 'People';
  public personList: Array<IPerson>;
  public showing = 0;
  public personCount: number;

  ngOnInit() {
    this.personListSubscription = this.phs.getPersonList().subscribe(this.phs.personListNextCallback, this.phs.personListErrorCallback);
    this.phs.receivedData.subscribe(() => {
      this.personCount = this.phs.personCount;
      this.personList = this.phs.personList;
    })
  }

  ngOnDestroy() {
    if (this.personListSubscription) {
      this.personListSubscription.unsubscribe();
    }
  }

  public setDisplay(index: number) {
    this.showing = 10 * index;
  }

}
