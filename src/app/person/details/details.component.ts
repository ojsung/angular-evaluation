import { Component, OnInit, OnDestroy } from '@angular/core';
import { PersonHttpService } from '../person-http.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { IDetails } from './details';
import { IPerson } from '../person/person';
import { FormGroup, AbstractControl, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnDestroy {

  constructor(private phs: PersonHttpService, private ar: ActivatedRoute) { }
  public pageTitle = 'Card and Person Info'
  public details: Array<IDetails>;
  public person: IPerson;
  public formDisplayed = false;
  private person_id: number;
  private personIndex: number;
  private personDetailsSubscription: Subscription;
  private personListSubscription: Subscription;
  private paramSubscription: Subscription;
  public cardForm: FormGroup;
  private cardControl: AbstractControl;

  // A callback for when the details are being retrieved.  Sets the card list
  private setDetails = details => {
    const detailList = details as Array<IDetails>;
    this.details = detailList;
  }

  ngOnInit() {
    // Start by retrieving the parameters from the route, and filling out the page data
    this.paramSubscription = this.ar.params.subscribe(route => {
      const routeParams = route.id
      this.person_id = parseInt(routeParams, 10);
      this.personIndex = this.person_id - 1;
      if (this.phs.personList && this.phs.personList.length) {
        this.person = this.phs.personList[this.personIndex]
      } else {
        this.getPersonList();
      }
    });
    this.personDetailsSubscription = this.phs.getPersonDetails(this.person_id).subscribe(this.setDetails);

    // Create the form for entering the card number
    this.cardForm = new FormGroup({
      card: new FormControl('', [Validators.required, Validators.minLength(16), Validators.maxLength(16)])
    });
    this.cardControl = this.cardForm.get('card');
  }

  ngOnDestroy() {
    // Destroy any subscriptions
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.personDetailsSubscription) {
      this.personDetailsSubscription.unsubscribe();
    }
    if (this.personListSubscription) {
      this.personListSubscription.unsubscribe();
    }
  }

  public addCard() {
    const cardNumber = this.cardControl.value;
    this.phs.addCard(this.person_id, cardNumber).subscribe(card => {
      this.details.push(card)
    });
  }

  public showForm() {
    this.formDisplayed = !this.formDisplayed;
  }

  // Only called if the personList isn't already filled in the person http service
  private getPersonList() {
    this.personListSubscription = this.phs.getPersonList().subscribe(this.phs.personListNextCallback, this.phs.personListErrorCallback);
    this.phs.receivedData.subscribe(() => {
      this.person = this.phs.personList[this.personIndex];
    });
  }
}
