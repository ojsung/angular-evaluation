import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'card'
})
export class CardPipe implements PipeTransform {

  transform(value: string, ...args: any[]): any {
    return value.slice(0, 4) + '-' + value.slice(4,8) + '-' + value.slice(8,12) + '-' + value.slice(12,17)
  }
}
