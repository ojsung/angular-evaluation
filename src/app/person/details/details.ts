export interface IDetails {
  id?: number;
  card_number: string; // card_number being a string gives me a good laugh
  person_id: number;
  balance?: number;
}
