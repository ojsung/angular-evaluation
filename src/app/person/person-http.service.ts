import { Injectable, EventEmitter, Output } from '@angular/core'
import { HttpService } from '../shared/http.service'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { IPerson } from './person/person'
import { IDetails } from './details/details'

@Injectable({
  providedIn: 'root'
})
export class PersonHttpService extends HttpService {
  constructor(protected http: HttpClient) {
    super(http)
  }
  @Output() receivedData: EventEmitter<true> = new EventEmitter()
  public personList: Array<IPerson>
  public personCount: number
  private personListEndpoint = 'person/list'
  private personDetailsEndpoint = 'card/query'
  private addCardEndpoint = 'card/add'
  private error: any

  // The callback to fill out the data in the person http service
  public personListNextCallback = person => {
    const personArray = person as Array<IPerson>
    this.personList = personArray
    this.personList = personArray
    this.personCount = this.personList.length
    this.receivedData.emit(true)
  }

  // Set error to true if there are any errors
  // Since there aren't any routes set up to log errors, will just
  // console log for now
  public personListErrorCallback = err => {
    this.error = err as any
    // do something with the error
    console.log(this.error)
  }

  public getPersonList(): Observable<IPerson> {
    return this.postRequest(this.personListEndpoint, {})
  }

  public getPersonDetails(person_id: number): Observable<IDetails> {
    const body = { person: person_id }
    return this.postRequest(this.personDetailsEndpoint, body)
  }

  public addCard(person_id: number, card_number: string): Observable<IDetails> {
    const body: IDetails = { person_id, card_number }
    return this.postRequest(this.addCardEndpoint, body)
  }
}
