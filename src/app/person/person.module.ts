import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonComponent } from './person/person.component';
import { DetailsComponent } from './details/details.component';
import { PersonRoutingModule } from './person-routing.module';
import { EnumeratorPipe } from './enumerator.pipe';
import { CardPipe } from './details/card.pipe'
import { ReactiveFormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    PersonComponent,
    DetailsComponent,
    EnumeratorPipe,
    CardPipe,
  ],
  imports: [
    CommonModule,
    PersonRoutingModule,
    ReactiveFormsModule
  ]
})
export class PersonModule { }
