import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

export class HttpService {
  constructor(protected http: HttpClient) {}
  private route = 'http://localhost:3000'

  // There are no get routes, so this is not needed.  Force of habit.
  // protected getRequest(endpoint: string): Observable<any> {
  //   return this.http.get(`${this.route}/${endpoint}`, { responseType: 'json' });
  // }

  protected postRequest(endpoint: string, body: any): Observable<any> {
    return this.http.post(`${this.route}/${endpoint}`, body, { responseType: 'json' });
  }
}
